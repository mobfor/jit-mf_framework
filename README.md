# JIT-MF_Framework

This repo holds experiments carried out to polish the JIT-MF framework and subsequent tool: https://gitlab.com/mobfor/jit-mf  &  https://gitlab.com/mobfor/mobfor-project

Papers related to these experiments:
- https://locard.eu/attachments/article/120/real-time-triggering-of-android-memory-dumps-for-stealthy-attack-investigation.pdf
- https://arxiv.org/abs/2105.05510
- https://ieeexplore.ieee.org/document/9737464
